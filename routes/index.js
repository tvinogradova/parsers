var express = require('express');
var router = express.Router();
var htmlParser = require('../services/htmlParser');
var phantomParser = require('../services/phantomParser');


/* GET home page. */
router.get('/parse', function (req, res, next) {
    htmlParser.parsePage('/blogs/server', function (error, data) {
        if (error) {
            res.status(400).send(error)
        }
        else {
            res.status(200).send("ok")
        }

    })
});

router.get('/phantom-parse', function (req, res, next) {
    phantomParser.parsePage(function (error, data) {
        if (error) {
            res.status(400).send(error)
        }
        else {
            res.status(200).send("ok")
        }

    })
});

module.exports = router;
