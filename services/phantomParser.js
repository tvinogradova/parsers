var Horseman = require('node-horseman');

exports.parsePage = function (callback) {
    var horseman = new Horseman();
    horseman
        .userAgent('Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0')
        .open('http://localhost:3000')
        .waitForSelector('#loginButton')
        .html()
        .log()
        .type('#inputEmail', 'admin@gmail.com')
        .type('#inputPassword', '111111')
        .click('#loginButton')
        .waitForSelector('.blog')
        .html()
        .log()
        .close()
        .then(function(){
            callback()
        });
};