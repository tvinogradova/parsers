var htmlparser = require("htmlparser2");
var http = require('http');

exports.parsePage = function (path, callback) {
    var options = {
        host: 'localhost',
        port: 3000,
        path: path,
        method: 'GET'
    };
    var request = http.request(options, function (res) {
        var data = '';
        res.on('data', function (chunk) {
            data += chunk;
        });
        res.on('end', function () {
            try {
                var parser = new htmlparser.Parser({
                    onopentag: function (name, attribs) {
                        console.log("open tag", name, attribs)
                    },
                    ontext: function (text) {
                        console.log("text", text)
                    },
                    onclosetag: function (tagname) {
                        console.log("close tag", tagname)
                    },
                    onend: function () {
                        console.log("end");
                        callback()
                    }
                }, {decodeEntities: true});
                parser.write(data);
                parser.end();

            }
            catch (err) {
                console.log("error", err);
                callback(err);
            }

        });
    });
    request.on('error', function (e) {
        console.log("error", e.message);
    });
    request.end();
};